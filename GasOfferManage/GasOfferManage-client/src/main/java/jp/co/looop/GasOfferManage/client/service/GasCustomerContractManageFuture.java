package jp.co.looop.GasOfferManage.client.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import jp.co.looop.GasOfferManage.object.GasCustomerContractManageQo;

@Component
public class GasCustomerContractManageFuture {

	@Autowired
	private GasCustomerContractManageRestService gasCustomerContractManageService;

	@AsyncTimed
	public CompletableFuture<String> cancel(GasCustomerContractManageQo gasCustomerContractManageQo) {
		return CompletableFuture.supplyAsync(() -> {
			return gasCustomerContractManageService.cancel(gasCustomerContractManageQo);
		});
	}
}
