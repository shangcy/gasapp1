package jp.co.looop.GasOfferManage.client.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import jp.co.looop.GasOfferManage.object.GasCustomerContractManageQo;

@FeignClient("GasCustomerContractManage")
@RequestMapping(value = "${webui.GasCustomerContractManage}")
public interface GasCustomerContractManageClient {

	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, value = "/cancel")
	String cancel(@RequestBody GasCustomerContractManageQo gasCustomerContractManageQo);
}
