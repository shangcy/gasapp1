package jp.co.looop.GasOfferManage.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import jp.co.looop.GasOfferManage.object.GasCustomerContractManageQo;

@Service
public class GasCustomerContractManageRestService {

	@Autowired
	private GasCustomerContractManageClient gasCustomerContractManageClient;

	@HystrixCommand(fallbackMethod = "cancelFallback")
	@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "6000")
	public String cancel(GasCustomerContractManageQo gasCustomerContractManageQo) {
		return gasCustomerContractManageClient.cancel(gasCustomerContractManageQo);
	}
	
	public String cancelFallback(GasCustomerContractManageQo gasCustomerContractManageQo) {
		return "-1";
	}
}
