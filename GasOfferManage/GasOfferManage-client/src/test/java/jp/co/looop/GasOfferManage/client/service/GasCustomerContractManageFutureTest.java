package jp.co.looop.GasOfferManage.client.service;

import static org.mockito.ArgumentMatchers.any;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import jp.co.looop.GasOfferManage.client.service.GasCustomerContractManageFuture;
import jp.co.looop.GasOfferManage.client.service.GasCustomerContractManageRestService;
import jp.co.looop.GasOfferManage.object.GasCustomerContractManageQo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GasCustomerContractManageFutureTest {

	@Autowired
	private GasCustomerContractManageFuture gasCustomerContractManageFuture;

	@MockBean
	private GasCustomerContractManageRestService gasCustomerContractManageRestService;

	@Test
	public void givenGasCustomerContractManageQo_whenCancel_returnGasCustomerContractManageQo() {
		// given
		GasCustomerContractManageQo gasCustomerContractManageQo = new GasCustomerContractManageQo("A001", "1");
		Mockito.when(gasCustomerContractManageRestService.cancel(any(GasCustomerContractManageQo.class)))
				.thenReturn(gasCustomerContractManageQo.toString());
		Object exp = "GasCustomerContractManageQo(gasCustomerId=A001, gasOfferStatus=1)";
		// when
		CompletableFuture<Object> result = gasCustomerContractManageFuture.cancel(gasCustomerContractManageQo)
				.thenApply(fn -> {
					return fn.toString();
				});
		try {
		// then
			Assert.assertTrue(result.get().equals(exp));
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}
