package jp.co.looop.GasOfferManage.client.service;

import static org.mockito.ArgumentMatchers.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import jp.co.looop.GasOfferManage.client.service.GasCustomerContractManageClient;
import jp.co.looop.GasOfferManage.client.service.GasCustomerContractManageRestService;
import jp.co.looop.GasOfferManage.object.GasCustomerContractManageQo;

@RunWith(SpringRunner.class)
public class GasCustomerContractManageRestServiceTest {
	@TestConfiguration
	static class GasOfferManageServiceTestContextConfiguration {
		@Bean
		public GasCustomerContractManageRestService gasOfferManageService() {
			return new GasCustomerContractManageRestService();
		}
	}
	
	@Autowired
	private GasCustomerContractManageRestService gasCustomerContractManageRestService;

	@MockBean
	private GasCustomerContractManageClient gasCustomerContractManageClient;
	
	@Test
	public void validateCancel_returnGasCustomerContractManageQo() {
		// given
		GasCustomerContractManageQo gasCustomerContractManageQo = new GasCustomerContractManageQo("A001","1");
		Mockito.when(gasCustomerContractManageClient.cancel(any(GasCustomerContractManageQo.class)))
		.thenReturn(gasCustomerContractManageQo.toString());
		// when
		String result = gasCustomerContractManageRestService.cancel(gasCustomerContractManageQo);
		String exp = "GasCustomerContractManageQo(gasCustomerId=A001, gasOfferStatus=1)";
		// then
		Assert.assertEquals(exp, result);
	}
	
	@Test
	public void validateCancel_returnMinusOne() {
		// given
		GasCustomerContractManageQo gasCustomerContractManageQo = new GasCustomerContractManageQo("A001","1");
		// when
		String result = gasCustomerContractManageRestService.cancelFallback(gasCustomerContractManageQo);
		String exp = "-1";
		// then
		Assert.assertEquals(exp, result);
	}
}
