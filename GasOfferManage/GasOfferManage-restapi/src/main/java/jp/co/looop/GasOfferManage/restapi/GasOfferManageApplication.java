package jp.co.looop.GasOfferManage.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
@EnableHystrix
@EnableFeignClients(basePackages = "jp.co.looop")
@ComponentScan(basePackages = "jp.co.looop")
public class GasOfferManageApplication {
	public static void main(String[] args) {

		SpringApplication.run(GasOfferManageApplication.class, args);

	}
}
