package jp.co.looop.GasOfferManage.restapi.controller;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import jp.co.looop.GasOfferManage.client.service.GasCustomerContractManageRestService;
import jp.co.looop.GasOfferManage.domain.entity.GasOffer;
import jp.co.looop.GasOfferManage.domain.service.GasOfferManageService;
import jp.co.looop.GasOfferManage.object.GasCustomerContractManageQo;
import jp.co.looop.GasOfferManage.object.GasOfferManageQo;

@RestController
@RequestMapping("/GasOffer")
public class GasOfferManageController {

	private static Logger logger = LoggerFactory.getLogger(GasOfferManageController.class);
	@Autowired
	private GasCustomerContractManageRestService gasCustomerContractManageRestService;

	@Autowired
	private GasOfferManageService gasOfferManageService;

	/**
	 * 「ガス需要家ID」によりガス申込DB情報を取得する
	 * 
	 * @param gasCustomerId
	 * @return
	 */
	@RequestMapping(value = "/getInfo/{gasCustomerId}")
	public CompletableFuture<String> findByGasCustomerId(@PathVariable String gasCustomerId) {
		return CompletableFuture.supplyAsync(() -> {
			GasOfferManageQo gomQo = new GasOfferManageQo();
			GasOffer gasOffer = gasOfferManageService.findByGasCustomerId(gasCustomerId);
			if (gasOffer != null) {
				BeanUtils.copyProperties(gasOffer, gomQo);
			}
			return new Gson().toJson(gomQo);
		});
	}

	/**
	 * 「ガス需要家ID」に対するガス申込DBの契約状態を「申込キャンセル」に更新する
	 * 
	 * @param gasOfferManageQo
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/cancel")
	public CompletableFuture<String> cancel(@RequestBody GasOfferManageQo gasOfferManageQo) throws Exception {
		return CompletableFuture.supplyAsync(() -> {
			GasOffer gasOffer = new GasOffer();
			BeanUtils.copyProperties(gasOfferManageQo, gasOffer);
			gasOffer.setGasOfferStatus("1");
			GasOffer gasOfferRes = gasOfferManageService.upsert(gasOffer);
			if (gasOfferRes == null) {
				logger.info("ガス申込情報申込キャンセルが失敗しました。");
				return null;
			} else {
				logger.info("ガス申込情報申込キャンセル  -> ガス需要家ID = " + gasOfferRes.getGasCustomerId());
				GasCustomerContractManageQo gccmQo = new GasCustomerContractManageQo();
				gccmQo.setGasCustomerId(gasOfferManageQo.getGasCustomerId());
				gccmQo.setGasOfferStatus("1");
				String sId = gasCustomerContractManageRestService.cancel(gccmQo);
				if (sId == null) {
					logger.info("ガス需要家契約情報申込キャンセルが失敗しました。");
					return null;
				} else if (sId.equals("-1")) {
					logger.info("ガス需要家契約情報申込サービスが利用できません。");
					return "-1";
				} else {
					return sId;
				}
			}
		});
	}
}
