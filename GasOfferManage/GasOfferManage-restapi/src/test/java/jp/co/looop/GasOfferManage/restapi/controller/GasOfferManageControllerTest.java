package jp.co.looop.GasOfferManage.restapi.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.co.looop.GasOfferManage.client.service.GasCustomerContractManageRestService;
import jp.co.looop.GasOfferManage.domain.entity.GasOffer;
import jp.co.looop.GasOfferManage.domain.service.GasOfferManageService;
import jp.co.looop.GasOfferManage.object.GasCustomerContractManageQo;
import jp.co.looop.GasOfferManage.object.GasOfferManageQo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GasOfferManageControllerTest {

	private MockMvc mockMvc;

	@MockBean
	private GasOfferManageService gasOfferManageService;
	
	@MockBean
	private GasCustomerContractManageRestService gasCustomerContractManageRestService;
	
	@Autowired
	private GasOfferManageController gasOfferManageController;

	@Autowired
	private ObjectMapper objectMapper;
	
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(gasOfferManageController).build();
	}

	@Test
	public void whenFindByGasCustomerId_thenReturnNg() throws Exception {
		// when
		mockMvc.perform(get("/GasOffer/getInfo/{gasCustomerId}", "")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void givenGasOffer_whenFindByGasCustomerId_thenReturnJson() throws Exception {
		// given
		GasOffer gasOffer = new GasOffer();
		gasOffer.setGasCustomerId("A001");
		gasOffer.setGasOfferStatus("0");
		Mockito.when(gasOfferManageService.findByGasCustomerId(Mockito.anyString())).thenReturn(gasOffer);
		// when
		MvcResult mvcResult = mockMvc.perform(get("/GasOffer/getInfo/{gasCustomerId}", "A001")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		// then
		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		String tr = (String) mvcResult.getAsyncResult();
		String er = "{\"gasCustomerId\":\"A001\",\"gasOfferStatus\":\"0\"}";
		JSONAssert.assertEquals(er, tr, true);
	}

	@Test
	public void givenNull_whenFindByGasCustomerId_thenReturnNull() throws Exception {
		// given
		Mockito.when(gasOfferManageService.findByGasCustomerId(Mockito.anyString())).thenReturn(null);
		// when
		MvcResult mvcResult = mockMvc.perform(get("/GasOffer/getInfo/{gasCustomerId}", "A001")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		// then
		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		String tr = (String) mvcResult.getAsyncResult();
		String er = "{}";
		JSONAssert.assertEquals(er, tr, true);
	}

	@Test
	public void givenGasOffer_whenCancel_thenReturnNullOfDomainService() throws Exception {
		// given
		GasOffer gasOffer = new GasOffer();
		gasOffer.setGasCustomerId("A001");
		gasOffer.setGasOfferStatus("1");
		Mockito.when(gasOfferManageService.upsert(any(GasOffer.class))).thenReturn(null);

		GasOfferManageQo gasOfferManageQo = new GasOfferManageQo();
		gasOfferManageQo.setGasCustomerId("A001");
		gasOfferManageQo.setGasOfferStatus("0");
		// when
		MvcResult mvcResult = mockMvc.perform(put("/GasOffer/cancel")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(gasOfferManageQo)))
				.andReturn();
		// then
		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
	    Assert.assertEquals(mvcResult.getAsyncResult(), null);
	}
	
	@Test
	public void givenGasOffer_whenCancel_thenReturnNullOfClientRestService() throws Exception {
		// given
		GasOffer gasOffer = new GasOffer();
		gasOffer.setGasCustomerId("A001");
		gasOffer.setGasOfferStatus("1");
		
		GasOfferManageQo gasOfferManageQo = new GasOfferManageQo();
		gasOfferManageQo.setGasCustomerId("A001");
		gasOfferManageQo.setGasOfferStatus("0");
		
		Mockito.when(gasOfferManageService.upsert(any(GasOffer.class))).thenReturn(gasOffer);
		Mockito.when(gasCustomerContractManageRestService.cancel(any(GasCustomerContractManageQo.class))).thenReturn(null);
		
		// when
		MvcResult mvcResult = mockMvc.perform(put("/GasOffer/cancel")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(gasOfferManageQo)))
				.andReturn();
		// then
		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertEquals(mvcResult.getAsyncResult(), null);
	}
	
	@Test
	public void givenGasOffer_whenCancel_thenReturnMinusOneOfClientRestService() throws Exception {
		// given
		GasOffer gasOffer = new GasOffer();
		gasOffer.setGasCustomerId("A001");
		gasOffer.setGasOfferStatus("1");
		
		GasOfferManageQo gasOfferManageQo = new GasOfferManageQo();
		gasOfferManageQo.setGasCustomerId("A001");
		gasOfferManageQo.setGasOfferStatus("0");
		
		Mockito.when(gasOfferManageService.upsert(any(GasOffer.class))).thenReturn(gasOffer);
		Mockito.when(gasCustomerContractManageRestService.cancel(any(GasCustomerContractManageQo.class))).thenReturn("-1");
		
		// when
		MvcResult mvcResult = mockMvc.perform(put("/GasOffer/cancel")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(gasOfferManageQo)))
				.andReturn();
		// then
		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertEquals(mvcResult.getAsyncResult(), "-1");
	}

	@Test
	public void givenGasOffer_whenCancel_thenReturnString() throws Exception {
		// given
		GasOffer gasOffer = new GasOffer();
		gasOffer.setGasCustomerId("A001");
		gasOffer.setGasOfferStatus("1");
		
		GasOfferManageQo gasOfferManageQo = new GasOfferManageQo();
		gasOfferManageQo.setGasCustomerId("A001");
		gasOfferManageQo.setGasOfferStatus("0");
		
		Mockito.when(gasOfferManageService.upsert(any(GasOffer.class))).thenReturn(gasOffer);
		Mockito.when(gasCustomerContractManageRestService.cancel(any(GasCustomerContractManageQo.class))).thenReturn("A001");
		
		// when
		MvcResult mvcResult = mockMvc.perform(put("/GasOffer/cancel")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(gasOfferManageQo)))
				.andReturn();
		// then
		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertEquals(mvcResult.getAsyncResult(), "A001");
	}
}
