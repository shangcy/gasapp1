package jp.co.looop.GasOfferManage.restapi.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.co.looop.GasOfferManage.client.service.GasCustomerContractManageRestService;
import jp.co.looop.GasOfferManage.domain.entity.GasOffer;
import jp.co.looop.GasOfferManage.domain.repository.GasOfferManageRepository;
import jp.co.looop.GasOfferManage.object.GasCustomerContractManageQo;
import jp.co.looop.GasOfferManage.object.GasOfferManageQo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GasOfferManageControllerIntegrationTest {

	private MockMvc mockMvc;
	
	@Autowired
	private GasOfferManageController gasOfferManageController;

	@Autowired
	private GasOfferManageRepository gasOfferManageRepository;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private GasCustomerContractManageRestService gasCustomerContractManageRestService;
	
	@Before
	public void setup() {
		// given
		// 1.search method of data
		GasOffer gasOfferForGet = new GasOffer();
		gasOfferForGet.setGasCustomerId("A002");
		gasOfferForGet.setGasOfferStatus("0");
		gasOfferManageRepository.save(gasOfferForGet);
		// 2.cancel method of data
		GasOffer gasOfferForCancel = new GasOffer();
		gasOfferForCancel.setGasCustomerId("A001");
		gasOfferForCancel.setGasOfferStatus("0");
		gasOfferManageRepository.save(gasOfferForCancel);
		this.mockMvc = MockMvcBuilders.standaloneSetup(gasOfferManageController).build();
	}

	@Test
	public void givenGasOffer_whenFindByGasCustomerId_thenReturnJson() throws Exception {
		// when
		MvcResult mvcResult = mockMvc.perform(get("/GasOffer/getInfo/{gasCustomerId}", "A002")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		// then
		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		String tr = (String) mvcResult.getAsyncResult();
		String er = "{\"gasCustomerId\":\"A002\",\"gasOfferStatus\":\"0\"}";
		JSONAssert.assertEquals(er, tr, true);
	}
	
	@Test
	public void givenGasOffer_whenCancel_thenReturnString() throws Exception {
		
		GasOfferManageQo gasOfferManageQo = new GasOfferManageQo();
		gasOfferManageQo.setGasCustomerId("A001");
		gasOfferManageQo.setGasOfferStatus("0");
		Mockito.when(gasCustomerContractManageRestService.cancel(any(GasCustomerContractManageQo.class))).thenReturn("A001");
		
		// when
		MvcResult mvcResult = mockMvc.perform(put("/GasOffer/cancel")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(gasOfferManageQo)))
				.andReturn();
		// then
		Assert.assertEquals(200, mvcResult.getResponse().getStatus());
		Assert.assertEquals(mvcResult.getAsyncResult(), "A001");
	}
}
