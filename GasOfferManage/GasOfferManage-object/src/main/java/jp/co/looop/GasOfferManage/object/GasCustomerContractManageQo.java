package jp.co.looop.GasOfferManage.object;

import lombok.Data;

@Data
public class GasCustomerContractManageQo {
	private String gasCustomerId;
	private String gasOfferStatus;

	public GasCustomerContractManageQo() {
	}

	public GasCustomerContractManageQo(String gasCustomerId, String gasOfferStatus) {
		this.gasCustomerId = gasCustomerId;
		this.gasOfferStatus = gasOfferStatus;
	}

}
