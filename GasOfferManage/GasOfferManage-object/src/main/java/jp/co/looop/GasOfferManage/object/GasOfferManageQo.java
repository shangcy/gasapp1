package jp.co.looop.GasOfferManage.object;

import lombok.Data;

@Data
public class GasOfferManageQo {

	private String gasCustomerId;
	private String gasOfferStatus;

	public GasOfferManageQo(String gasCustomerId, String gasOfferStatus) {
		this.gasCustomerId = gasCustomerId;
		this.gasOfferStatus = gasOfferStatus;
	}

	public GasOfferManageQo() {
	}

}
