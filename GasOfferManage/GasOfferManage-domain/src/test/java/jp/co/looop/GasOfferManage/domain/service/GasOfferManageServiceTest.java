package jp.co.looop.GasOfferManage.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import jp.co.looop.GasOfferManage.domain.entity.GasOffer;
import jp.co.looop.GasOfferManage.domain.repository.GasOfferManageRepository;

@RunWith(SpringRunner.class)
public class GasOfferManageServiceTest {
	@TestConfiguration
	static class GasOfferManageServiceTestContextConfiguration {
		@Bean
		public GasOfferManageService gasOfferManageService() {
			return new GasOfferManageService();
		}
	}

	@Autowired
	private GasOfferManageService gasOfferManageService;

	@MockBean
	private GasOfferManageRepository gasOfferManageRepository;

	@Test
	public void whenFindByGasCustomerId_returnGasOffer() {
		GasOffer gasOffer = new GasOffer();
		gasOffer.setGasCustomerId("A001");
		gasOffer.setGasOfferStatus("0");
		Mockito.when(gasOfferManageRepository.findByGasCustomerId(Mockito.anyString())).thenReturn(gasOffer);
		GasOffer result = gasOfferManageService.findByGasCustomerId(gasOffer.getGasCustomerId());
		assertThat(result.getGasCustomerId()).isEqualTo("A001");
		assertThat(result.getGasOfferStatus()).isEqualTo("0");
	}

	@Test
	public void validateSave_returnGasOffer() {
		GasOffer gasOffer = new GasOffer();
		gasOffer.setGasCustomerId("A112");
		gasOffer.setGasOfferStatus("0");
		Mockito.when(gasOfferManageRepository.save(any(GasOffer.class))).thenReturn(gasOffer);
		GasOffer result = gasOfferManageService.upsert(gasOffer);
		assertThat(result.getGasCustomerId()).isEqualTo("A112");
		assertThat(result.getGasOfferStatus()).isEqualTo("0");
	}
}
