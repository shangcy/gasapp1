package jp.co.looop.GasOfferManage.domain.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import jp.co.looop.GasOfferManage.domain.entity.GasOffer;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GasOfferManageRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private GasOfferManageRepository gasOfferManageRepository;

	@Test
	public void whenFindByGasCustomerId_returnGasOffer() {
		// given
		GasOffer gasOffer = new GasOffer();
		gasOffer.setGasCustomerId("A001");
		gasOffer.setGasOfferStatus("0");
		entityManager.persist(gasOffer);
		entityManager.flush();

		// when
		GasOffer result = gasOfferManageRepository.findByGasCustomerId(gasOffer.getGasCustomerId());

		// then
		assertThat(result.getGasCustomerId()).isEqualTo(gasOffer.getGasCustomerId());
		assertThat(result.getGasOfferStatus()).isEqualTo(gasOffer.getGasOfferStatus());

	}
}
