package jp.co.looop.GasOfferManage.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jp.co.looop.GasOfferManage.domain.entity.GasOffer;

@Repository
public interface GasOfferManageRepository extends JpaRepository<GasOffer, Long>, JpaSpecificationExecutor<GasOffer> {

	GasOffer findByGasCustomerId(@Param("gasCustomerId") String gasCustomerId);
}
