package jp.co.looop.GasOfferManage.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_GasOffer")
public class GasOffer implements java.io.Serializable {

	private static final long serialVersionUID = 2078287408335683505L;

	@Id
	private String gasCustomerId;
	private String gasOfferStatus;

	public String getGasCustomerId() {
		return gasCustomerId;
	}

	public void setGasCustomerId(String gasCustomerId) {
		this.gasCustomerId = gasCustomerId;
	}

	public String getGasOfferStatus() {
		return gasOfferStatus;
	}

	public void setGasOfferStatus(String gasOfferStatus) {
		this.gasOfferStatus = gasOfferStatus;
	}
}
