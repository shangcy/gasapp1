package jp.co.looop.GasOfferManage.domain.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.looop.GasOfferManage.domain.entity.GasOffer;
import jp.co.looop.GasOfferManage.domain.repository.GasOfferManageRepository;

@Service
@Transactional
public class GasOfferManageService {

	@Autowired
	private GasOfferManageRepository gasOfferManageRepository;
	
	public GasOffer findByGasCustomerId(String gasCustomerId) {
		return gasOfferManageRepository.findByGasCustomerId(gasCustomerId);
	}
	
	public GasOffer upsert(GasOffer gasOffer) {
		return gasOfferManageRepository.save(gasOffer);
	}
}
